### Tenisu
This is a Java Spring Boot demo project about tennis players.  

## Locally
Run tests with  
```mvn clean test```  

Run with   
```./mvnw spring-boot:run```  

Access on port 8090 : http://localhost:8090/

Endpoints :
- [/players](http://localhost:8090/players)  
- [/players/{id}](http://localhost:8090/players/17)  
- [/statistics](http://localhost:8090/statistics)  



### CI/CD
TODO
